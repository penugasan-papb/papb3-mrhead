package com.example.barikhead;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    // Declare Variables
    ImageView body, eyes, beard, hair, eyebrow, moustache;
    CheckBox hairCheck, eyebrowCheck, janggutCheck, kumisCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Variables
        body = findViewById(R.id.body);
        eyes = findViewById(R.id.eyes);
        beard = findViewById(R.id.beard);
        hair = findViewById(R.id.hair);
        eyebrow = findViewById(R.id.eyebrow);
        moustache = findViewById(R.id.moustache);
        hairCheck = findViewById(R.id.hairCheck);
        eyebrowCheck = findViewById(R.id.eyebrowCheck);
        janggutCheck = findViewById(R.id.eyebrowBeard);
        kumisCheck = findViewById(R.id.moustacheCheck);

        // Hair Check Method
        hairCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Determine Visibility
                boolean visible = !hair.isShown();
                hair.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

        // Eyebrow Check Method
        eyebrowCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Determine Visibility
                boolean visible = !eyebrow.isShown();
                eyebrow.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

        // Janggut (Beard) Check Method
        janggutCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Determine Visibility
                boolean visible = !beard.isShown();
                beard.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

        // Kumis (Moustache) Check Method
        kumisCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Determine Visibility
                boolean visible = !moustache.isShown();
                moustache.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });
    }
}
